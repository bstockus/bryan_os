// Code based on PCI implementation here: 
//  https://github.com/gamozolabs/chocolate_milk/blob/master/kernel/src/pci.rs

// MIT License
//
// Copyright (c) 2020, Gamozo Labs, LLC
// Copyright (c) 2020, Bryan Stockus
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

use crate::println;

use core::mem::size_of;

/// Locatability of PCI Memory BAR
#[derive(Clone, Copy)]
#[repr(u32)]
pub enum BarMemoryLocatableType {
    /// 32-bit BAR
    Bits32 = 0,

    /// <1 MiB BAR (obsolete)
    LessThan1MiB = 1,

    /// 64-bit BAR
    Bits64 = 2,
}

impl From<u32> for BarMemoryLocatableType {
    fn from(val: u32) -> Self {
        match val {
            0 => BarMemoryLocatableType::Bits32,
            1 => BarMemoryLocatableType::LessThan1MiB,
            2 => BarMemoryLocatableType::Bits64,
            _ => panic!("Invalid BAR Locatability Type"),
        }
    }
}

/// Prefetchability of PCI Memory BAR
#[derive(Clone, Copy)]
#[repr(u32)]
pub enum BarMemoryPrefetchableType {
    /// Is Not Prefetchable
    No = 0,

    /// Is Prefetchable
    Yes = 1
}

impl From<u32> for BarMemoryPrefetchableType {
    fn from(val: u32) -> Self {
        match val {
            0 => BarMemoryPrefetchableType::No,
            1 => BarMemoryPrefetchableType::Yes,
            _ => panic!("Invalid BAR Prefetchability Type")
        }
    }
}

/// I/O port for the PCI configuration space window address
const PCI_CONFIG_ADDRESS: u16 = 0xcf8;

/// I/O port for the PCI configuration space window data
const PCI_CONFIG_DATA: u16 = 0xcfc;

/// Enable bit for accessing the `0xcf8` I/O port
const PCI_ADDRESS_ENABLE: u32 = 1 << 31;

/// Common PCI header for the PCI configuration space of any device or bridge
#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct PciHeader {
    pub vendor_id: u16,
    pub device_id: u16,
    pub command: u16,
    pub status: u16,
    pub revision: u8,
    pub prog_if: u8,
    pub subclass: u8,
    pub class: u8,
    pub cache_line_size: u8,
    pub latency_timer: u8,
    pub header_type: u8,
    pub bist: u8,
}

/// Configuration space for a PCI device
#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct PciDevice {
    /// Standard PCI configuration space header
    pub header: PciHeader,

    pub bar0: u32,
    pub bar1: u32,
    pub bar2: u32,
    pub bar3: u32,
    pub bar4: u32,
    pub bar5: u32,
    pub cardbus_cis_pointer: u32,
    pub subsystem_vendor_id: u16,
    pub subsystem_device_id: u16,
    pub expansion_rom_address: u32,
    pub capabilities: u8,
    pub reserved: [u8; 7],
    pub interrupt_line: u8,
    pub interrupt_pin: u8,
    pub min_grant: u8,
    pub max_latency: u8,
}

fn pci_config_read32(addr: u32) -> u32 {
    use x86_64::instructions::port::Port;

    let mut pci_config_addr_port = Port::new(PCI_CONFIG_ADDRESS);
    let mut pci_config_data_port = Port::new(PCI_CONFIG_DATA);

    unsafe { pci_config_addr_port.write(addr) };
    let result: u32 = unsafe{ pci_config_data_port.read() };
    return result;
}

fn pci_bdf_to_address(bus: u8, device: u8, function: u8) -> u32 {
    assert!(device < 32, "Device ID is out of range");
    assert!(function < 8, "Function ID is out of range");
    return ((bus as u32) << 8) 
            | ((device as u32) << 3) 
            | ((function as u32) << 0);
}

fn pci_addr_to_bdf(addr: u32) -> (u8, u8, u8) {
    let bus: u8 = ((addr >> 16) & 0b1111_1111) as u8;
    let device: u8 = ((addr >> 11) & 0b1_1111) as u8;
    let function: u8 = ((addr >> 8) & 0b111) as u8;
    return (bus, device, function);
}

pub fn init_pci() {

    let mut pci_enum = [0u64; 256 * 32 * 8 / 64];

    // For each possible bus ID
    for bus in 0..=255 {
        // For each possible device ID
        for device in 0..32 {
            // For each possible function ID
            for function in 0..8 {
                // Compute the address to select this BDF combination
                let pci_addr = pci_bdf_to_address(bus, device, function);

                // Compute the PCI selection address
                let addr = PCI_ADDRESS_ENABLE | (pci_addr << 8);

                // Select the address and read the device and vendor ID
                let did_vid: u32 = pci_config_read32(addr);


                if did_vid != 0xffff_ffff {
                    println!("Device Found [{:#02x}:{:#02x}:{:#01x}]",
                        bus,
                        device,
                        function);
                    // Set the device present in the PCI enumeration table
                    let idx = pci_addr / 64;
                    let bit = pci_addr % 64;
                    pci_enum[idx as usize] |= 1 << bit;
                }
            }
        }
    }

    for (idx, &pci_map) in pci_enum.iter().enumerate() {
        // No devices here, go to the next `u64`
        if pci_map == 0 {
            continue;
        }

        // There is at least 1 device in this set
        for bit in 0..64 {
            // If the bit is not set, no device here, skip it
            if (pci_map & (1u64 << bit)) == 0 { continue; }

            // Compute the PCI address for this bit
            let pci_addr = (idx * 64) | bit;

            // Compute the PCI selection address
            let addr = PCI_ADDRESS_ENABLE | (pci_addr << 8) as u32;

            // Read the PCI configuration header
            let mut header =
                [0u32; size_of::<PciHeader>() / size_of::<u32>()];
            for (rid, register) in header.iter_mut().enumerate() { 
                // Set the window to the register we want to read and read the
                // value
                *register = pci_config_read32(addr | (rid * size_of::<u32>()) 
                                as u32);
            }

            // Convert the header to our `PciHeader` structure
            let header: PciHeader = unsafe { core::ptr::read_unaligned(
                header.as_ptr() as *const PciHeader) };

            // Skip non-device PCI entries (skips things like PCI bridges)
            if (header.header_type & 0x7f) != 0 {
                continue;
            }

            // Read the PCI configuration
            let mut device = [0u32; size_of::<PciDevice>() / 
                                        size_of::<u32>()];
            for (rid, register) in device.iter_mut().enumerate() { 
                // Set the window to the register we want to read and read the
                // value
                *register = pci_config_read32(addr | (rid * size_of::<u32>()) 
                                as u32);
            }

            // Convert the device to our `PciDevice` structure
            let device: PciDevice = unsafe { core::ptr::read_unaligned(
                device.as_ptr() as *const PciDevice) };
            
            let (pci_bus, pci_device, pci_function) = 
                    pci_addr_to_bdf(addr);

            println!("PCI [{:#02x}:{:#02x}:{:#01x}] | {:#06x}:{:#06x} | {:#06x}:{:#06x}",
                pci_bus,
                pci_device,
                pci_function,
                device.header.vendor_id,
                device.header.device_id,
                device.subsystem_vendor_id,
                device.subsystem_device_id);

            println!("  BAR0: [{:#04x}]\n  BAR1: [{:#04x}]\n  BAR2: [{:#04x}]",
                     device.bar0,
                     device.bar1,
                     device.bar2);

            println!("  BAR3: [{:#04x}]\n  BAR4: [{:#04x}]\n  BAR5: [{:#04x}]",
                     device.bar3,
                     device.bar4,
                     device.bar5);


        }
    }

}
