# Homebuilt x86_64 Operating System written in Rust

Based of this source: [https://os.phil-opp.com]

## Commands to Setup Dev Environment

1. Install QEMU x86-64 system for your platform.
2. Install bootimage cargo tool:
        ```> cargo install bootimage```
3. Install LLVM Preview Tools:
        ```> rustup component add llvm-tools-preview```